Template.top_nav.events({
	"click .js-toggle-messages" : ()=>{
		if(Session.get('showChats')){
			Session.set('showChats', false);
			Session.set('showCards', true)
		}
		else{
			Session.set('showChats', true);
			Session.set('showCards', false);
			Session.set('threadOpened', false);
		}
	},
	"click .js-toggle-notifications" : ()=>{
		if(Session.get('showNotification')){
			Session.set('showCards', true);
			Session.set('showNotification', false);
			Session.set('showChats',false);
			Session.set('threadOpened',false);
		}
		else{
			Session.set('showCards', false);
			Session.set('showNotification', true);
			Session.set('showChats',false);
			Session.set('threadOpened',false);
		}
	}
});

Template.top_nav.helpers({
	"activeChats": function(){
		return Session.get('showChats');
	},
	"activeNotifs": function(){
		return Session.get('showNotification');
	}
});
