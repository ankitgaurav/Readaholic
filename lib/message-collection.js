Messages = new Mongo.Collection("messages");
Meteor.methods({
	"addMessage": function(toUser, content){
		const newMessage = {
	      toUser: toUser,
	      fromUser: Meteor.userId(),
	      content: content,
	      createdAt: new Date()
	    };
	    Messages.insert(newMessage, function(error,result){
	      if (error) {
	          console.log("There was some error inserting the new message")
	        }
		});
	}
});