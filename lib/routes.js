FlowRouter.route('/', {
  action: function() {
    BlazeLayout.render("mainLayout", {feed: "feed", sidebar: "sidebar"});
  },
  name: 'home'
});
FlowRouter.route('/profile/:user', {
    action: function(params) {
        BlazeLayout.render("mainLayout", {feed: "profile", sidebar: "sidebar"});
    },
    name: "userprofile"
});
FlowRouter.route('/explore', {
  name: "explore",
  action: function() {
      BlazeLayout.render("mainLayout", {feed: "explore", sidebar: "sidebar"});
  }
});
FlowRouter.route('/post/:postId', {
    action: function(params) {
        BlazeLayout.render("mainLayout", {feed: "singlePost"});
    },
    name: "post"
});
FlowRouter.notFound = {
  action() {
    BlazeLayout.render('mainLayout', {feed: 'page_not_found', sidebar: "sidebar"});
  }
};