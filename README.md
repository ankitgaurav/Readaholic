# Why Readaholic

+ Your intellect  finds acceptance from world
+ Become a super blogger without learning to optimize your website
+ Make khan academy social
+ Show off your knowledge instead of Photo
+ Build your circle of friends and followers to discuss specific issues
+ Now wake up everyday to learn things instead of just watching selfies or memes
+ blogs , posts  , status, tutorials , website contents  contents
+ Socializing online shouldn't feel like work.

# Key Terminoloy

1. Readaholic
2. Reads
3. Circles
4. Likes
5. Topics

# css rules

* instead of camel case use '-' for multi word classes

# Javascipt rules

+ Use camelCase for variable names
+ no explicit "class" keyword, function to define a class
+ class function doubles as constructor for objects
+ use === instead of == and !== instead of != for equality checking
+ avoid the use of "with"
+ Start private variables with an underscore "_"
+ avoid using eval
+ prefer fat arrows of ES6 =>