Accounts.onCreateUser(function(options, user) {
 UserCustom.insert({
  name:user.username,
  password:user.services.password.bcrypt,
  userId:user._id,
  bio: null,
  followers:[],
  following:[user._id],
  friends:[],
  url:  user.username.replace(/ /g,"_")
 });
 return user;
});

// Deny all client-side updates on the collections
Posts.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
UserComments.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
UserCustom.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
